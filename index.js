module.exports = {
  env: {
    node: true,
  },
  "globals": {
    "Dokbot": false
  },
  "parserOptions": {
    "parser": "babel-eslint"
  },
  extends: ['plugin:vue/essential', '@vue/prettier'],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'prettier/prettier': [
      'warn',
      {
        singleQuote: true,
        semi: false,
        trailingComma: 'all',
        htmlWhitespaceSensitivity: 'ignore',
        printWidth: 100,
      },
    ],
  },
}
